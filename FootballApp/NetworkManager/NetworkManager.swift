//
//  NetworkManager.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import Foundation
import UIKit

class NetworkManager {
    
    static let shared = NetworkManager()
    
    let urlPathPlayers = "https://api-football-beta.p.rapidapi.com/players/topscorers"
    let urlPathTeams = "https://api-football-beta.p.rapidapi.com/teams"

//    let apiKey = "251b4eb8c7mshc148c7ffba6d3c8p1ff79fjsnc7edda17d876"
    let apiKeySecond = "b6d52306admsh2ac65f39c116260p1c3d5ejsnf1a1d40f92f0"
    let apiHost = "api-football-beta.p.rapidapi.com"

//MARK: function loading Players
    func getPlayers(season: String, league: String, completionHandler: @escaping ([PlayerInfo]?, String?) -> Void) {
        let urlComponent = URLComponents(string: urlPathPlayers)
        
        guard var urlComponent = urlComponent else { return }
        
        let queryItemLeague = URLQueryItem(name: "league", value: league)
        let queryItemSeason = URLQueryItem(name: "season", value: season)
        
        urlComponent.queryItems = [queryItemSeason, queryItemLeague]
        
        guard let url = urlComponent.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": apiHost, "x-rapidapi-key": apiKeySecond]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(PlayersResponseModel.self, from: data)
                completionHandler(response.response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
            
        }.resume()
    }
    
//MARK: function loading Teams
    func getTeam(teamID: String, completionHandler: @escaping ([TeamInfo]?, String?) -> Void) {
        let urlComponent = URLComponents(string: urlPathTeams)
        
        guard var urlComponent = urlComponent else { return }
        
        let queryItemTeamID = URLQueryItem(name: "id", value: teamID)
        
        urlComponent.queryItems = [queryItemTeamID]
        
        guard let url = urlComponent.url else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": apiHost, "x-rapidapi-key": apiKeySecond]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(nil, error?.localizedDescription)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(TeamsResponseModel.self, from: data)
                completionHandler(response.response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }.resume()
    }
    
//MARK: function loading Image
    func getImage(urlImage: String, photoView: UIImageView) {
        
        DispatchQueue.global().async {
            
            if let url = URL(string: urlImage) {
                let task = URLSession.shared.dataTask(with: url) { data, response, error in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async {
                        photoView.image = UIImage(data: data)
                    }
                }
                task.resume()
            }
        }
    }
}
