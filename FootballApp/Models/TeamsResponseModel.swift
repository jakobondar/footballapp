//
//  TeamsResponseModel.swift
//  FootballApp
//
//  Created by Яков on 30.10.2021.
//

import Foundation

struct TeamsResponseModel: Codable {
    var response: [TeamInfo]
}

struct TeamInfo: Codable {
    var team: Team
    var venue: Venue
}

struct Team: Codable {
    var id: Int
    var name: String
    var country: String
    var founded: Int
    var national: Bool
    var logo: String
}

struct Venue: Codable {
    var id: Int
    var name: String
    var address: String
    var city: String
    var capacity: Int
    var surface: String
    var image: String
}
