//
//  PlayerModel.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import Foundation

// MAIN MODEL
struct PlayersResponseModel: Codable {
    let errors: [Error]
    let response: [PlayerInfo]
}

struct Error: Codable {
    let massage: String
}

struct PlayerInfo: Codable {
    let player: Player
    let statistics: [Statistics]
}

struct Statistics: Codable {
    var team: PlayerTeam
}

struct PlayerTeam: Codable {
    var id: Int
    var logo: String
    var name: String
}

struct Player: Codable {
    let id: Int
    let age: Int
    let firstname: String
    let height: String
    let injured: Bool
    let lastname: String
    let name: String
    let nationality: String
    let photo: String
    let weight: String
    let birth: PlayerBirthday
}

struct PlayerBirthday: Codable {
    let country: String
    let date: String
    let place: String
}

