//
//  ShowAlertErrorMessage.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import UIKit

class AlertPopUpView {
    
    static let shared = AlertPopUpView()
    
    func showAlert(title: String? = nil, message: String? = nil, buttonTitle: String? = nil) -> UIAlertController {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: title, style: .cancel, handler: nil)
        
        alertVC.addAction(okAction)
        return alertVC
    }
}
