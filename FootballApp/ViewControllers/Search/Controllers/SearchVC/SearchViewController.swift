//
//  SearchViewController.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var leagueTextField: UITextField!
    @IBOutlet weak var seasonTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leagueTextField.delegate = self
        seasonTextField.delegate = self
    }
    
    deinit { debugPrint("🏁 - \(classForCoder)") }
    
    @IBAction func viewAction(_ sender: Any) {
        showInfoPlayers(leagueTextField: leagueTextField, seasonTextField: seasonTextField)
    }
}
