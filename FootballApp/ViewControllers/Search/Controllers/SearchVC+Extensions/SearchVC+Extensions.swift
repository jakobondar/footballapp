//
//  SearchVC+Extensions.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import Foundation
import UIKit

//MARK: - function - activating the button next to the textField
extension SearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case leagueTextField:
            seasonTextField.becomeFirstResponder()
        default:
            seasonTextField.resignFirstResponder()
            showInfoPlayers(leagueTextField: leagueTextField, seasonTextField: seasonTextField)
        }
        return true
    }
}

//MARK: - functions - show Info Players
extension SearchViewController {
    
    func showInfoPlayers(leagueTextField: UITextField, seasonTextField: UITextField) {

        if checkingTextFields(leagueTextField, seasonTextField){
            let playersListVC = PlayersListViewController(nibName: "PlayersListViewController", bundle: nil)
            playersListVC.season = seasonTextField.text ?? "2019"
            playersListVC.league = "39"
            navigationController?.pushViewController(playersListVC, animated: true)
        } else {
            present(AlertPopUpView.shared.showAlert(title: "Erroe", message: "fill in all the text fields", buttonTitle: "Ok"), animated: true, completion: nil)
        }
    }
    
    func checkingTextFields(_ leagueTextField: UITextField, _ seasonTextField: UITextField) -> (Bool) {

        if !(leagueTextField.text == "") && !(seasonTextField.text == "") && !(leagueTextField.text == nil) && !(seasonTextField.text == nil) {
            return true
        }
        return false
    }
}
