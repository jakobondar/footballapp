//
//  PlayersListTableViewCell.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import UIKit

protocol PlayersListDelegate: AnyObject {
    func showInfo(player: PlayerInfo, typeInfo: TypeInfo)
}

class PlayersListTableViewCell: UITableViewCell {
    
    weak var delegate: PlayersListDelegate?
    
    var currentPlayer: PlayerInfo = .init(player: Player(id: 18788,
                                                         age: 0,
                                                         firstname: "",
                                                         height: "",
                                                         injured: false,
                                                         lastname: "",
                                                         name: "J. Vardy",
                                                         nationality: "England",
                                                         photo: "https://media.api-sports.io/football/players/18788.png",
                                                         weight: "",
                                                         birth: PlayerBirthday(country: "", date: "", place: "")),
                                          statistics: [Statistics(team: PlayerTeam(id: 46,
                                                                                   logo: "",
                                                                                   name: ""))])

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var playerPhotoImageView: UIImageView!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var taemNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(player: PlayerInfo) {
        currentPlayer = player
        playerNameLabel.text = player.player.name
        taemNameLabel.text = player.statistics[0].team.name
        
        NetworkManager.shared.getImage(urlImage: player.player.photo, photoView: playerPhotoImageView)
        NetworkManager.shared.getImage(urlImage: player.statistics[0].team.logo, photoView: logoImageView)
    }
    
    @IBAction func showPlayerInfoAction(_ sender: Any) {
        delegate?.showInfo(player: currentPlayer, typeInfo: .infoPlayer)
    }
    
    @IBAction func showTeamInfoAction(_ sender: Any) {
        delegate?.showInfo(player: currentPlayer, typeInfo: .infoTeam)
    }
}
