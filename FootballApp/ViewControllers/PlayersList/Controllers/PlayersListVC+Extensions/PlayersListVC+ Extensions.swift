//
//  PlayersListVC+ Extensions.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import UIKit

extension PlayersListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerslList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playersListCellID, for: indexPath) as! PlayersListTableViewCell
        cell.update(player: playerslList[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension PlayersListViewController: PlayersListDelegate {
    
    func showInfo(player: PlayerInfo, typeInfo: TypeInfo) {
        
        let detailedInfoVC = DetailedInfoViewController(nibName: "DetailedInfoViewController", bundle: nil)
        detailedInfoVC.currentPlayer = player
        detailedInfoVC.typeInfo = typeInfo
        present(detailedInfoVC, animated: true, completion: nil)
    }
}
