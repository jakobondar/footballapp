//
//  PlayersListViewController.swift
//  FootballApp
//
//  Created by Яков on 29.10.2021.
//

import UIKit

class PlayersListViewController: UIViewController {

    let playersListCellID = String(describing: PlayersListTableViewCell.self)
    var league: String = ""
    var season: String = ""
    var playerslList = [PlayerInfo]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: playersListCellID, bundle: nil), forCellReuseIdentifier: playersListCellID)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NetworkManager.shared.getPlayers(season: season, league: league) { playersGlobal, error in
            guard error == nil else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let playersGlobal = playersGlobal else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.playerslList = playersGlobal

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    deinit { debugPrint("🏁 - \(classForCoder)") }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
