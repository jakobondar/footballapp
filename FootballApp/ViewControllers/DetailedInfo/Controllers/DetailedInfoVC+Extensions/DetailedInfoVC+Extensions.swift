//
//  DetailedInfoVC+Extensions.swift
//  FootballApp
//
//  Created by Яков on 31.10.2021.
//

import UIKit

enum TypeInfo {
    case infoPlayer
    case infoTeam
}

extension DetailedInfoViewController {
    
    func showDetailedInfo(currentPlayer: PlayerInfo, typeInfo: TypeInfo) {
        
        switch typeInfo {
        case .infoPlayer:
            
            titleLabel.text = "Карточка игрока"
            NetworkManager.shared.getImage(urlImage: currentPlayer.player.photo, photoView: infoImageView)
            nameTitleLabel.text = currentPlayer.player.firstname + " " + currentPlayer.player.lastname
            ageFoundedTitleLabel.text = "Возраст:"
            ageFoundedLabel.text = "\(currentPlayer.player.age)"
            nationalityCountryTitleLabel.text = "Национальность:"
            nationalityCountryLabel.text = currentPlayer.player.nationality
            heightView.isHidden = false
            heightTitleLabel.text = "Рост:"
            heightLabel.text = currentPlayer.player.height
            nationalityViewToBirthViewConstraints.constant = 45
            birthVenueTitleLabel.text = "День рождения:"
            dataNameVenueTitleLabel.text = "Дата:"
            dataNameVenueLabel.text = currentPlayer.player.birth.date
            countryCityTitleLabel.text = "Страна:"
            countryCityLabel.text = currentPlayer.player.birth.country

        case .infoTeam:
            
            NetworkManager.shared.getTeam(teamID: "\(currentPlayer.statistics[0].team.id)") { team, error in
                guard error == nil else {
                    self.present(AlertPopUpView().showAlert(title: "ERROR", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                    return
                }
                
                guard let team = team?[0] else {
                    self.present(AlertPopUpView().showAlert(title: "ERROR", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                    return
                }

                DispatchQueue.main.async {
                    
                    self.titleLabel.text = "Команда"
                    NetworkManager.shared.getImage(urlImage: team.venue.image, photoView: self.infoImageView)
                    NetworkManager.shared.getImage(urlImage: team.team.logo, photoView: self.logoImageView)
                    self.nameTitleLabel.text = team.team.name
                    self.ageFoundedTitleLabel.text = "Основана:"
                    self.ageFoundedLabel.text = "\(team.team.founded)"
                    self.nationalityCountryTitleLabel.text = "Страна:"
                    self.nationalityCountryLabel.text = team.team.country
                    self.heightView.isHidden = true
                    self.nationalityViewToBirthViewConstraints.constant = 5
                    self.birthVenueTitleLabel.text = "Стадион:"
                    self.dataNameVenueTitleLabel.text = "Название:"
                    self.dataNameVenueLabel.text = team.venue.name
                    self.countryCityTitleLabel.text = "Город:"
                    self.countryCityLabel.text = team.venue.city
                    
                    
                }
            }
        }
    }
}
