//
//  DetailedInfoViewController.swift
//  FootballApp
//
//  Created by Яков on 30.10.2021.
//

import UIKit

class DetailedInfoViewController: UIViewController {

    var league: String = "39"
    var season: String = "2019"
    var teamslList = [TeamInfo]()
    var currentPlayer: PlayerInfo = .init(player: Player(id: 18788,
                                                         age: 0,
                                                         firstname: "",
                                                         height: "",
                                                         injured: false,
                                                         lastname: "",
                                                         name: "J. Vardy",
                                                         nationality: "England",
                                                         photo: "https://media.api-sports.io/football/players/18788.png",
                                                         weight: "",
                                                         birth: PlayerBirthday(country: "", date: "", place: "")),
                                          statistics: [Statistics(team: PlayerTeam(id: 46,
                                                                                   logo: "",
                                                                                   name: ""))])
    var typeInfo: TypeInfo = .infoTeam
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var ageFoundedTitleLabel: UILabel!
    @IBOutlet weak var ageFoundedLabel: UILabel!
    @IBOutlet weak var nationalityCountryTitleLabel: UILabel!
    @IBOutlet weak var nationalityCountryLabel: UILabel!
    @IBOutlet weak var heightView: UIView!
    @IBOutlet weak var heightTitleLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var nationalityViewToBirthViewConstraints: NSLayoutConstraint!
    @IBOutlet weak var birthVenueTitleLabel: UILabel!
    @IBOutlet weak var dataNameVenueTitleLabel: UILabel!
    @IBOutlet weak var dataNameVenueLabel: UILabel!
    @IBOutlet weak var countryCityTitleLabel: UILabel!
    @IBOutlet weak var countryCityLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showDetailedInfo(currentPlayer: currentPlayer, typeInfo: typeInfo)
    }
    
    deinit { debugPrint("🏁 - \(classForCoder)") }
    
    @IBAction func backAction(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
}
